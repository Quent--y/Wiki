<article>
	<div class="block">
		<p><?= t('Version du');?> : <?=format_date($data['time']);?> (<a href="<?=$CONFIG['root'];?>?do=history&id=<?=$data['slug'];?>"><?= t('Historique');?></a>)</p>
	</div>
	<header>
		<h1><?=$data['title'];?></h1>
	</header>
	<main><?=$content;?></main>
	<textarea rows=25"><?=$data['content'];?></textarea>
</article>
