��    2      �  C   <      H  !   I     k     y     �  #   �     �     �     �  	             '     ,     C     P     Y     y  
   �     �     �     �     �     �     �     �                    /     A     T     t     �     �     �     �     �     �  
   �            	   $     .     >  ,   K     x     �     �     �  +   �  �  �     �  
   �     �     �     	          !     /  	   F     P     a     f     |     �  %   �     �     �  	   �  
   �     �     �     �                 	   &  
   0     ;     I  %   X     ~  	   �     �     �     �     �     �     �               !  
   *  
   5  +   @     l     {     �  
   �     �             +                 #          *      %              .   &                    -          "                       (                
          '   	             /      2   ,       0                    )   $      1   !                Activer la réécriture d’URL ? Aujourd’hui Autoriser la libre lecture ? Changements récents Choix de la langue de l’interface Choix du fuseau horaire Configuration Configuration générale Connexion Conçu avec %s Date Dernière modification Déconnexion Exporter Feuille de style personnalisée Fichiers Historique Il y a %d ans Il y a %d heures Il y a %d minutes Il y a %d mois Il y a %d secondes Il y a %d semaine Il y a %s jours Il y a un an Il y a un mois Il y a une heure Il y a une minute Il y a une semaine Impossible de créer le fichier Le titre de votre Wiki Modifier Nom de la page d’accueil Personnalisation Plan du site Racine de votre wiki Recherche pour « %s » Rechercher Sauvegarder Se souvenir de moi Supprimer Téléversement Téléverser Une nouvelle version est disponible (%s, %s) Votre courriel la locale existe la locale n'existe pas À l’instant Érreur le fichier n’est pas téléversé Project-Id-Version: 
PO-Revision-Date: 2023-08-05 16:45+0200
Last-Translator: 
Language-Team: 
Language: occitan
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 3.0.1
X-Poedit-Basepath: ../..
X-Poedit-SearchPath-0: app/templates/article.php
X-Poedit-SearchPath-1: app/templates/atom.php
X-Poedit-SearchPath-2: app/templates/config.php
X-Poedit-SearchPath-3: app/templates/edit.php
X-Poedit-SearchPath-4: app/templates/history.php
X-Poedit-SearchPath-5: app/templates/history-compare.php
X-Poedit-SearchPath-6: app/templates/history-view.php
X-Poedit-SearchPath-7: app/templates/index.php
X-Poedit-SearchPath-8: app/templates/install.php
X-Poedit-SearchPath-9: app/templates/login.php
X-Poedit-SearchPath-10: app/templates/search.php
X-Poedit-SearchPath-11: app/templates/upload.php
X-Poedit-SearchPath-12: app/inc/app.php
X-Poedit-SearchPath-13: app/inc/functions.php
X-Poedit-SearchPath-14: index.php
 Activar la reescritura d'URL ? Fa un jorn Autorizar la liura lectura ? Cambiament recents Lenga Vostre flux orari Configuracion Configuracion generala Connexion Propulsat per %s Data Darriera modificacion Desconnexion Exportar Personalizacion de la fuèlha d'estil Nom Istoric Fa %d ans Fa %d oras Fa %d minutas Fa %d meses Fa %d segondas Fa %d setmanas Fa %s jorns Fa un an Fa un mes Fa una ora Fa una minuta Fa una setmana Error: impossible de crear lo fichier Lo tiedtol del site Modificar Nom de la pagina d'acuèlh Personalizacion Plan del site L'adreça del site Recèrca per « %s » Cercar Salvagardar Resta connectat Suprimir Transmetre Transmetre Una nova mesa a jorn es disponibla (%s, %s) Vostre corriel la locala existís la locala existís pas Ara meteis Error : impossible d'enviar 