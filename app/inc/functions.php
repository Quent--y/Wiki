<?php
####### Fonctions internes ###############
function check_newversion() {
    global $config;
    $new_version = file_get_contents('https://framagit.org/qwertygc/Wiki/raw/master/VERSION');
    $current_version = file_get_contents('VERSION');
    if ($new_version > $current_version) {
        return '<div class="block">'.a('https://framagit.org/qwertygc/Wiki', sprintf(t("Une nouvelle version est disponible (%s, %s)"), $current_version, $new_version)).'</div>';
    }
}
function write_article($slug, $title, $content, $comment, $openread) {
	sql('INSERT INTO article(slug, title, content, time, ip, comment, openread) VALUES (?, ?, ?, ?, ?, ?, ?);', [$slug, $title, $content, time(), $_SERVER['REMOTE_ADDR'], $comment, $openread]);
}
function read_article($slug) {
	global $db;
	$query = $db->prepare('SELECT * FROM article WHERE slug=? ORDER BY time DESC LIMIT 1');
	$query->execute([$slug]);
	$data = $query->fetch(PDO::FETCH_ASSOC);
	return $data;
}
function checkconfigkey($values) {
    global $db;
    foreach ($values as $key=>$value) {
        $query = sql('SELECT COUNT(`key`) as nb FROM config WHERE `key`=?', array($key));
        $data = $query->fetch();
        if ($data['nb'] > 0) {
            // Existe, balek frere
        } else {
            sql('INSERT INTO `config`(`key`, `value`) VALUES (?, ?)', array($key, $value));
        }
    }
}
#### Base de données et fonctions SQL ####

function sql($query='', $values=array()) {
    global $db;
    $query = $db->prepare($query);
    $query->execute($values);
    return $query;
}


##### Utilitaires #######


function ZipDir($filename, $path) {
	$rootPath = realpath($path);
	$zip = new ZipArchive();
	$zip->open($filename, ZipArchive::CREATE | ZipArchive::OVERWRITE);
	$files = new RecursiveIteratorIterator(
		new RecursiveDirectoryIterator($rootPath),
		RecursiveIteratorIterator::LEAVES_ONLY
	);

	foreach ($files as $name => $file) {
		if (!$file->isDir()) {
			$filePath = $file->getRealPath();
			$relativePath = substr($filePath, strlen($rootPath) + 1);
			$zip->addFile($filePath, $relativePath);
		}
	}
	$zip->close();
}

function url($id) {
	global $CONFIG;
    if ($CONFIG['urlrewrite'] == true) {
        $url = $CONFIG['root'].$id;
    } else {
        $url = $CONFIG['root'].'?id='.$id;
    }
    return $url;
}


function redirect($url) {
	@header('Location: '.$url);
	echo '<meta http-equiv="refresh" content="0; url='.$url.'">';
	die;
}
function toslug($string) {
    $table = array(
            'Š'=>'S', 'š'=>'s', 'Đ'=>'Dj', 'đ'=>'dj', 'Ž'=>'Z', 'ž'=>'z', 'Č'=>'C', 'č'=>'c', 'Ć'=>'C', 'ć'=>'c',
            'À'=>'A', 'Á'=>'A', 'Â'=>'A', 'Ã'=>'A', 'Ä'=>'A', 'Å'=>'A', 'Æ'=>'A', 'Ç'=>'C', 'È'=>'E', 'É'=>'E',
            'Ê'=>'E', 'Ë'=>'E', 'Ì'=>'I', 'Í'=>'I', 'Î'=>'I', 'Ï'=>'I', 'Ñ'=>'N', 'Ò'=>'O', 'Ó'=>'O', 'Ô'=>'O',
            'Õ'=>'O', 'Ö'=>'O', 'Ø'=>'O', 'Ù'=>'U', 'Ú'=>'U', 'Û'=>'U', 'Ü'=>'U', 'Ý'=>'Y', 'Þ'=>'B', 'ß'=>'Ss',
            'à'=>'a', 'á'=>'a', 'â'=>'a', 'ã'=>'a', 'ä'=>'a', 'å'=>'a', 'æ'=>'a', 'ç'=>'c', 'è'=>'e', 'é'=>'e',
            'ê'=>'e', 'ë'=>'e', 'ì'=>'i', 'í'=>'i', 'î'=>'i', 'ï'=>'i', 'ð'=>'o', 'ñ'=>'n', 'ò'=>'o', 'ó'=>'o',
            'ô'=>'o', 'õ'=>'o', 'ö'=>'o', 'ø'=>'o', 'ù'=>'u', 'ú'=>'u', 'û'=>'u', 'ý'=>'y', 'ý'=>'y', 'þ'=>'b',
            'ÿ'=>'y', 'Ŕ'=>'R', 'ŕ'=>'r', '/' => '', ' ' => ''
    );
    $stripped = preg_replace(array('/\s{2,}/', '/[\t\n]/'), ' ', $string);
    return strtolower(strtr($string, $table));
}
function beautiful_timezone_list() {
    $tz = timezone_identifiers_list();
    $array = array();
    foreach ($tz as $v) {
        $codename = $v;
        $v = str_replace('_', ' ', $v);
        $v = str_replace('/', ' - ', $v);
        $array[$codename] = $v;
    }
    return $array;
}

function generateRandomString($length = 10) {
    $characters = '234567ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

function timeAgo($time) {
    $timestamp = time()-$time;
    $s = array(
                    'year' 		=> floor($timestamp/31536000),
                    'month'		=> floor($timestamp/2628000),
                    'week'		=> floor($timestamp/604800),
                    'day'		=> floor($timestamp/86400),
                    'hour'		=> floor($timestamp/3600),
                    'minute'	=> floor($timestamp/60),
                    'second'	=> floor($timestamp/1));

    if ($s['year'] >= 1) {
        return ($s['year'] > 1) ? sprintf(t('Il y a %d ans'), $s['year']) : sprintf(t('Il y a un an'), $s['year']);
    } elseif ($s['month'] >= 1) {
        return ($s['month'] > 1) ? sprintf(t('Il y a %d mois'), $s['month']) : sprintf(t('Il y a un mois'), $s['month']);
    } elseif ($s['week'] >= 1) {
        return ($s['week'] > 1) ? sprintf(t('Il y a %d semaine'), $s['week']) : sprintf(t('Il y a une semaine'), $s['week']);
    } elseif ($s['day'] >= 1) {
        return ($s['day'] > 1) ? sprintf(t('Il y a %s jours'), $s['day']) : sprintf(t('Aujourd’hui'), $s['day']);
    } elseif ($s['hour'] >= 1) {
        return ($s['hour'] > 1) ? sprintf(t('Il y a %d heures'), $s['hour']) : sprintf(t('Il y a une heure'), $s['hour']);
    } elseif ($s['minute'] >= 1) {
        return ($s['minute'] > 1) ? sprintf(t('Il y a %d minutes'), $s['minute']) : sprintf(t('Il y a une minute'), $s['minute']);
    } elseif ($s['second'] >= 1) {
        return ($s['second'] > 1) ? sprintf(t('Il y a %d secondes'), $s['second']) : sprintf(t('À l’instant'), $s['second']);
    } else {
        return sprintf(t('À l’instant'), $s['second']);
    }
}


function format_date($time) {
    return '<time datetime="'.date('c', $time).'" title="'.date('c', $time).'">'.timeAgo($time).'</time>';
}
function int_to_alph($int) {
	$alph = null;
	$chrs = array('0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S' ,'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '-', '_');
	$base = sizeof($chrs);
	do {
		$alph = $chrs[($int % $base)] . $alph;
	}
	while($int = intval($int / $base));
	return $alph;
}

function testlocale($lang) {
	@putenv("LANGUAGE=".$lang);
	 if(setlocale(LC_ALL, $lang) == $lang) {
	 	return $lang.' '.t('la locale existe');
	 }
	 else {
	 	return $lang.' '.t('la locale n\'existe pas');
	 }
}
function t($string) {
	global $CONFIG;
	if($CONFIG['gettext'] == 1) {
		return t($string);
	}
	else {
		$lang = json_decode(file_get_contents('app/locales/'.$CONFIG['lang'].'/LC_MESSAGES/main.json'), true);
		if(isset($lang['messages'][''][$string])) {
			return $lang['messages'][''][$string];
		}
		else {
			return $string;
		}

	}
}
#### Connexion #########
function check($var) {
    if ((isset($var) || !empty($var)) and $var == true) {
        return true;
    } else {
        return false;
    }
}
function authcookie() {
    global $CONFIG;
    if (isset($_COOKIE['auth']) && !isset($_SESSION['login'])) {
        if ($_COOKIE['auth'] == sha1($config['pwd'])) {
            $_SESSION['login'] = true;
            $_SESSION['token'] = md5(uniqid(rand(), true));
        } else {
            unset($_COOKIE['auth']);
            setcookie('auth', '', time() - 3600, null, null, false, true);
        }
    }
} 
function checklogin() {
    if (check(@$_SESSION['login'])) {
        return true;
    } else {
        return false;
    }
}


function checkOTP($otp) {
    global $CONFIG;
    $decalage = $CONFIG['otp_gap'];  // (en secondes)
    $maintenant = time() + $decalage;
    $totp = OTPHP\TOTP::create($CONFIG['otp_token']);
    return $totp->verify($otp, $maintenant);
}

function auth_otp($otp) {
    global $CONFIG;
    if ($CONFIG['otp_enable'] == true) {
        return checkOTP($otp);
    } else {
        return true;
    }
}



function Size($path) { #http://stackoverflow.com/questions/5501427/php-filesize-mb-kb-conversion
    $bytes = sprintf('%u', filesize($path));
    if ($bytes > 0) {
        $unit = intval(log($bytes, 1024));
        $units = array('B', 'KB', 'MB', 'GB');
        if (array_key_exists($unit, $units) === true) {
            $size = sprintf('%d %s', $bytes / pow(1024, $unit), $units[$unit]);
        }
    }

    return array('bytes'=>$bytes, 'size'=>$size);
}

####### Medias ################

function media($media) {
    $picture_mimetype = array(
        'image/apng',
        'image/bmp',
        'image/gif',
        'image/x-icon',
        'image/jpeg',
        'image/png',
        'image/svg+xml',
        'image/tiff',
        'image/webp',
    );
    $audio_mimetype = array(
        'audio/aac',
        'audio/midi',
        'audio/ogg',
        'audio/mp3',
        'audio/mpeg',
        'audio/x-wav',
        'audio/webm',
        'audio/3gpp',
        'audio/3gpp3'
    );
    $video_mimetype = array(
        'video/x-msvideo',
        'video/mpeg',
        'video/ogg',
        'video/webm',
        'video/mp4',
        'video/3gpp',
        'video/3gpp2',
        'video/x-matroska'
    );
    if (in_array(mime_content_type($media), $picture_mimetype)) {
        return 'picture';
    } elseif (in_array(mime_content_type($media), $video_mimetype)) {
        return 'video';
    } elseif (in_array(mime_content_type($media), $audio_mimetype)) {
        return 'audio';
    } else {
        return '';
    }
}

function affmedia($media) {
    if (media($media) == 'picture') {
        return '<img src="'.$media.'" style="max-height:100px;" alt="picture"/>';
    } elseif (media($media) == 'video') {
        return '<video controls src="'.$media.'" style="max-width:100%;"></video>';
    } elseif (media($media) == 'audio') {
        return '<audio controls src="'.$media.'" style="max-width:100%;"></audio>';
    } else {
        return '';
    }
}
function mdmedia($media) {
    if (media($media) == 'picture') {
        return '!['.$media.']('.$media.')';
    } else {
        return $media;
    }
}


