<?php
if (isset($_GET['do'])) {
    switch ($_GET['do']) {
    	case 'login':
    		if(isset($_SESSION['login'])) {
				unset($_SESSION['login']);
				unset($_SESSION['token']);
				unset($_COOKIE['auth']);
				setcookie('auth', '', time() - 3600, null, null, false, true);
				session_destroy();
				redirect($redirect);
				exit();
    		}
    		else {
    			include 'app/templates/login.php';
				if(isset($_POST['password_login'])) {
					$otp = ($CONFIG['otp_enable'] == true) ? $_POST['otp'] : null;
		            $otp = preg_replace('/\s+/', '', $otp);
					if($_POST['email'] == $CONFIG['email'] AND password_verify($_POST['password_login'], $CONFIG['pwd']) AND auth_otp($_POST['otp'])) {
						$_SESSION['login'] = true;
						$_SESSION['token'] = bin2hex(random_bytes(64));
						 if (isset($_POST['cookie'])) {
                            setcookie('auth', sha1($CONFIG['pwd']), time() + 30*24*3600, null, null, false, true);
                        }
						redirect(url($_POST['r']));
					}
					else {
						echo 'Mauvais mot de passe';
					}
				}
    		}
    	break;
    	case 'install':
			if(!file_exists(DATA_ROOT)) {
				if(isset($_POST['pwd'])) {
					mkdir('data', 0755);
					mkdir('data/files', 0755);
					$db = New PDO('sqlite:'.'data/data.db');
					$db->query('CREATE TABLE "article" (
					"id"	INTEGER PRIMARY KEY AUTOINCREMENT,
					"slug" TEXT,
					"title" TEXT,
					"content" TEXT,
					"time" TEXT,
					"comment" TEXT,
					"ip" TEXT,
					"openread" INTEGER);');
					write_article('index', 'Welcome ! It’s a new page', 'It’s a new page. Why not edit this page?', 'Creation', 1);
					$db->query('CREATE TABLE "config" ("id" INTEGER PRIMARY KEY AUTOINCREMENT,"key" TEXT, "value" TEXT);');
					checkconfigkey([
						'title'			=> 'My Wiki',
						'email'			=> $_POST['email'],
						'root'			=> str_replace('index.php', '', 'https://'.$_SERVER['SERVER_NAME'].$_SERVER['PHP_SELF']),
						'index'			=> 'index',
						'urlrewrite'	=> '1',
						'openread'		=> '1',
						'lang'			=> 'fr_FR',
						'timezone'		=> date_default_timezone_get(),
						'pwd'			=> password_hash($_POST['pwd'], PASSWORD_DEFAULT),	
						'otp_enable'	=> '0',
						'otp_gap'		=> '0',
						'otp_token'		=> generateRandomString(32),
						'otp_period'	=> 30,
						'gettext'		=> 0
					]);
					
					redirect('index.php');
				}
				else {
					$w = isset($_GET['w']) ? $_GET['w'] : '';
					include 'app/templates/install.php';
				}
			}
    	break;
    	case 'config':
    		if(checklogin()) {

				$urlrewrite = ($CONFIG['urlrewrite'] == '1') ? 'checked' : '';
				$openread = ($CONFIG['openread'] == '1') ? 'checked' : '';
				$otp_enable = ($CONFIG['otp_enable'] == '1') ? 'checked' : '';				
				if(!file_exists('data/style.css')) {file_put_contents('data/style.css', '');}
				$css = file_get_contents('data/style.css');
				
				$totp = OTPHP\TOTP::create($CONFIG['otp_token'], $CONFIG['otp_period'], 'sha1', 6);
				$totp->setLabel($CONFIG['email']);
				$totp->setIssuer($CONFIG['title']);
				$infototp = $totp->getProvisioningUri();
				$totpqrcode = $totp->getQrCodeUri('https://api.qrserver.com/v1/create-qr-code/?data='.$infototp.'&size=300x300&ecc=M',$infototp);
				$totpnow = $totp->now();

				$list_lang = array_diff(scandir('app/locales/', SCANDIR_SORT_DESCENDING), array('..', '.', 'main.pot'));
				
				include 'app/templates/config.php';
				
				
				if(isset($_GET['newtoken']) AND $_GET['token'] == $_SESSION['token']) {
					sql('UPDATE config SET value = ? WHERE key = "otp_token"', [generateRandomString(32)]);
					redirect('?do=config');
				}
				if(isset($_POST['email']) AND $_POST['token'] == $_SESSION['token']) {
					if($_POST['pwd'] != '') {
						sql('UPDATE config SET value = ? WHERE key = "pwd"', [password_hash($_POST['pwd'], PASSWORD_DEFAULT)]);
					}
					$urlrewrite = (isset($_POST['urlrewrite'])) ? '1' : '0';
					$openread = (isset($_POST['openread'])) ? '1' : '0';
					$otp_enable = (isset($_POST['otp_enable'])) ? '1' : '0';
					sql('UPDATE config SET value = ? WHERE key = "title"', [$_POST['title']]);
					sql('UPDATE config SET value = ? WHERE key = "index"', [$_POST['index']]);
					sql('UPDATE config SET value = ? WHERE key = "email"', [$_POST['email']]);
					sql('UPDATE config SET value = ? WHERE key = "root"', [$_POST['root']]);
					sql('UPDATE config SET value = ? WHERE key = "timezone"', [$_POST['timezone']]);
					sql('UPDATE config SET value = ? WHERE key = "lang"', [$_POST['lang']]);
					sql('UPDATE config SET value = ? WHERE key = "urlrewrite"', [$urlrewrite]);
					sql('UPDATE config SET value = ? WHERE key = "openread"', [$openread]);
					sql('UPDATE config SET value = ? WHERE key = "otp_enable"', [$otp_enable]);
					file_put_contents('data/style.css', $_POST['css']);

					redirect('?do=config');
				}
    		}
    	break;
    	case 'edit':
    		if(checklogin()) {
				$data = read_article($_GET['id']);
				$openread = ($CONFIG['openread'] == '1') ? 'checked' : '';
				if(!isset($data['slug'])) {
					$data['title'] = '';
					$data['content'] = '';
					$data['comment'] = 'Création';
					$openread = 1;
				}
				include 'app/templates/edit.php';

				if(isset($_POST['content']) AND $_SESSION['token'] == $_POST['token']) {
					$openread = (isset($_POST['openread'])) ? '1' : '0';
					write_article($_POST['slug'], $_POST['title'], $_POST['content'], $_POST['comment'], $openread);
					redirect(url($_POST['slug']));
				}

				if(isset($_POST['new_slug']) AND $_SESSION['token'] == $_POST['token']) {
					sql('UPDATE article SET slug=? WHERE slug=?', [toslug($_POST['new_slug']), $_POST['slug']]);
					redirect(url($_POST['new_slug']));
				}
				
				if(isset($_POST['del_slug']) AND ($_POST['del_slug'] == $_POST['slug']) AND $_SESSION['token'] == $_POST['token']) {
					sql('DELETE FROM `article` WHERE slug=?', [$_POST['del_slug']]);
					redirect(url($CONFIG['index']));
				}
    		}
    	break;
    	case 'index':
    		$query = $db->prepare('SELECT slug, max(time) as time, title, count(slug) as nb, openread FROM article GROUP BY slug ORDER BY slug ASC');
			$query->execute();
			$data = $query->fetchAll();
			include 'app/templates/index.php';	
    	break;
    	case 'history':
			if (isset($_GET['old']) && isset($_GET['new']) and !empty($_GET['old']) && !empty($_GET['new'])) {
				$query_new = $db->prepare('SELECT * FROM article WHERE id=?');
				$query_new->execute([$_GET['new']]);
				$new = $query_new->fetch();
				$query_old = $db->prepare('SELECT * FROM article WHERE id=?');
				$query_old->execute([$_GET['old']]);
				$old = $query_old->fetch();
				$compare = Jfcherng\Diff\DiffHelper::calculate($old['content'], $new['content'], 'Combined');
				include 'app/templates/history-compare.php';
			}
			elseif(isset($_GET['view'])) {
				$query = $db->prepare('SELECT * FROM article WHERE id=?');
				$query->execute([$_GET['view']]);
				$data = $query->fetch();
				if ((checklogin()|| check($CONFIG['openread'])) and (checklogin() || check($data['openread']))) {
					$content = $converter->convertToHtml($data['content']);
					include 'app/templates/history-view.php';
				}
			}
			else {
				if(isset($_GET['id'])) {
					$query = $db->prepare('SELECT * FROM article WHERE slug=? ORDER BY time DESC');
					$query->execute([$_GET['id']]);
					$data = $query->fetchAll();
				}
				else {
					$query = $db->prepare('SELECT * FROM article ORDER BY time DESC');
					$query->execute();
					$data = $query->fetchAll();
				}
				if(isset($_GET['restore']) AND $_GET['token'] == $_SESSION['token'] AND checklogin()) {
					$query = $db->prepare('SELECT * FROM article WHERE id=?');
					$query->execute([$_GET['restore']]);
					$data = $query->fetch();
					write_article($data['slug'], $data['title'], $data['content'], $data['comment'], $data['openread']);
					redirect('?do=history');
				}
				if(isset($_GET['delete']) AND $_GET['token'] == $_SESSION['token'] AND checklogin()) {
					sql('DELETE FROM article WHERE id= ?', [$_GET['delete']]);
					redirect('?do=history');
				}
				include 'app/templates/history.php';
			}
    	break;
    	case 'upload':
    		if(checklogin()) {
    			include 'app/templates/upload.php';
				 if (isset($_POST['upload']) AND $_SESSION['token'] == $_POST['token']) {
		                $_POST['overwrite'] = isset($_POST['overwrite']) ? true : false;
		                $files = array();
		                $fdata = $_FILES['file'];
		                $count = count($fdata['name']);
		                if (is_array($fdata['name'])) {
		                    for ($i=0;$i<$count;++$i) {
		                        $files[]=array(
		                            'name'     => int_to_alph(time()).'_'.$fdata['name'][$i],
		                            'tmp_name' => $fdata['tmp_name'][$i],
		                            'type' => mime_content_type($fdata['tmp_name'][$i]),
		                        );
		                    }
		                } else {
		                    $files[]=$fdata;
		                }
		                foreach ($files as $file) {
		                    if (!is_uploaded_file($file['tmp_name'])) {
		                        exit('<div class="block">'.t('Érreur le fichier n’est pas téléversé').'"</div>');
		                    }
		                    $file['name'] = ($_POST['overwrite'] == true) ? $file['name'] : time().$file['name'];
		                    if (!move_uploaded_file($file['tmp_name'], 'data/files/'.$file['name'])) {
		                        exit('<div class="block">'.t('Impossible de créer le fichier').'</div>');
		                    }
		                    echo '<input onclick="this.select()" value="'.mdmedia('data/files/'.$file['name']).'"/>';
		                }
		            }
		            if (isset($_GET['del']) and  $_GET['token'] == $_SESSION['token']) {
		                unlink('data/files/'.$_GET['del']);
		                redirect('?do=upload');
		            }
        	}
    	break;
    	case 'search':
    		include 'app/templates/search.php';
			if(isset($_POST['q'])) {
				echo sprintf(t('Recherche pour « %s »'), $_POST['q']);
				$query = $db->prepare('SELECT title, slug FROM article WHERE content LIKE ? GROUP BY slug');
				$query->execute(['%'.$_POST['q'].'%']);
				$data = $query->fetchAll();
				echo '<ul>';
				foreach($data as $search) {
					echo '<li><a href="'.url($search['slug']).'">'.$search['slug'].'</a></li>';
				}
				echo '</ul>';
			}
    	break;
    }
}
elseif(isset($_GET['id'])) {
	$data = read_article($_GET['id']);
	if ((checklogin()|| check($CONFIG['openread'])) and (checklogin() || check($data['openread']))) {
		if(isset($data['slug'])) {
			$content = $converter->convertToHtml($data['content']);
			include 'app/templates/article.php';
		}
		else {
			if(!in_array($_GET['id'], array('style.css'))) {
				write_article($_GET['id'], '', '', '', $CONFIG['openread']);
				redirect(url($_GET['id']));
			}
		}
	}
}
else {
	redirect(url($CONFIG['index']));
}
