<?php
error_reporting(E_NOTICE);
include 'app/inc/functions.php';
require 'vendor/autoload.php';

use Jfcherng\Diff\Differ;
use Jfcherng\Diff\DiffHelper;
use Jfcherng\Diff\Factory\RendererFactory;
use Jfcherng\Diff\Renderer\RendererConstant;

use OTPHP\TOTP;

#### Markdown
use League\CommonMark\Environment\Environment;
use League\CommonMark\Extension\CommonMark\CommonMarkCoreExtension;
use League\CommonMark\Extension\Attributes\AttributesExtension;
use League\CommonMark\Extension\Autolink\AutolinkExtension;
use League\CommonMark\Extension\DescriptionList\DescriptionListExtension;
use League\CommonMark\Extension\Footnote\FootnoteExtension;
use League\CommonMark\Extension\Strikethrough\StrikethroughExtension;
use League\CommonMark\Extension\HeadingPermalink\HeadingPermalinkExtension;
use League\CommonMark\Extension\TaskList\TaskListExtension;
use League\CommonMark\Extension\TableOfContents\TableOfContentsExtension;
use League\CommonMark\Extension\SmartPunct\SmartPunctExtension;
use League\CommonMark\Extension\Table\TableExtension;
use League\CommonMark\MarkdownConverter;

#### Translation

use Gettext\Loader\PoLoader;
use Gettext\Loader\JsonLoader;
use Gettext\Generator\JsonGenerator;
use Gettext\Translations;


$configmd = [
    'footnote' => [
        'backref_class'      => 'footnote-backref',
        'backref_symbol'     => '↩',
        'container_add_hr'   => true,
        'container_class'    => 'footnotes',
        'ref_class'          => 'footnote-ref',
        'ref_id_prefix'      => 'fnref:',
        'footnote_class'     => 'footnote',
        'footnote_id_prefix' => 'fn:'
        ],
      'table_of_contents' => [
        'html_class' => 'table-of-contents',
        'position' => 'top',
        'style' => 'bullet',
        'min_heading_level' => 1,
        'max_heading_level' => 6,
        'normalize' => 'relative',
        'placeholder' => null,
        ],
         'smartpunct' => [
        'double_quote_opener' => '“',
        'double_quote_closer' => '”',
        'single_quote_opener' => '‘',
        'single_quote_closer' => '’',
    ],
];

$environment = new Environment($configmd);
$environment->addExtension(new TableExtension());
$environment->addExtension(new CommonMarkCoreExtension());
$environment->addExtension(new AttributesExtension());
$environment->addExtension(new AutolinkExtension());
$environment->addExtension(new DescriptionListExtension());
$environment->addExtension(new FootnoteExtension());
$environment->addExtension(new StrikethroughExtension());
$environment->addExtension(new HeadingPermalinkExtension());
$environment->addExtension(new TableOfContentsExtension());
$environment->addExtension(new TaskListExtension());
$environment->addExtension(new SmartPunctExtension());
$converter = new MarkdownConverter($environment);

if (!file_exists('data') && $_GET['do'] != 'install') {
    redirect('index.php?do=install');
    $CONFIG = [
    	'root' => './',
    	'title' => 'Wiki'
    ];
    $title = '';
}
if (file_exists('data')) {
	$db = New PDO('sqlite:data/data.db');
	 $CONFIG = array();
    foreach ($db->query('SELECT * FROM config') as $data) {
        $CONFIG[$data['key']] = $data['value'];
    }
    $CONFIG['gettext'] = 0;
    if(isset($_GET['id']) AND $_GET['id'] != '') {
    	$query = sql('SELECT title FROM article WHERE slug=? ORDER BY time DESC LIMIT 1', [$_GET['id']]);
    	$data = $query->fetch();
    	$title = $data['title'];
    }
    else {
    	$title = '';
    }
    
	if (extension_loaded('gettext') AND $CONFIG['gettext'] == 1) {
 		if(setlocale(LC_ALL, $CONFIG['lang']) == $CONFIG['lang']) {
 		    setlocale(LC_ALL, $CONFIG['lang']);
			textdomain('main');
			bindtextdomain('main', "app/locales/");
			bind_textdomain_codeset('main', 'UTF-8');
 		}
	}
	else {
	}

    date_default_timezone_set($CONFIG['timezone']);
}

##########################################
#########################################
$_GET['r'] = isset($_GET['r']) ? $_GET['r'] : $_GET['id'];
