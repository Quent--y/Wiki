<?php
session_start();
include 'app/inc/inc.php';
authcookie();
?>
<!doctype html>
<html>
<head>
	<meta charset="utf-8"/>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
	<title><?=$CONFIG['title']; ?> - <?=$title; ?></title>
	<link rel="alternate" type="application/atom+xml" href="<?=$CONFIG['root']; ?>api.php?do=feed" title="Changements récents" />
	<link rel="stylesheet" href="<?=$CONFIG['root']; ?>app/assets/style.css">
	<link rel="stylesheet" href="https://unpkg.com/easymde/dist/easymde.min.css">
	<script src="https://unpkg.com/easymde/dist/easymde.min.js"></script>
	<link href="data/style.css" type="text/css" rel="stylesheet" />
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/katex@0.11.1/dist/katex.min.css" integrity="sha384-zB1R0rpPzHqg7Kpt0Aljp8JPLqbXI3bhnPWROx27a9N0Ll6ZP/+DiW/UqRcLbRjq" crossorigin="anonymous">
	<script defer src="https://cdn.jsdelivr.net/npm/katex@0.11.1/dist/katex.min.js" integrity="sha384-y23I5Q6l+B6vatafAwxRu/0oK/79VlbSz7Q9aiSZUvyWYIYsd+qj+o24G5ZU2zJz" crossorigin="anonymous"></script>
	<script defer src="https://cdn.jsdelivr.net/npm/katex@0.11.1/dist/contrib/auto-render.min.js" integrity="sha384-kWPLUVMOks5AQFrykwIup5lo0m3iMkkHrD0uJ4H5cjeGihAutqP0yW0J6dpFiVkI" crossorigin="anonymous" onload="renderMathInElement(document.body);"></script>
</head>
<body>
	<nav>
		<a href="<?=$CONFIG['root'].$CONFIG['index']; ?>" ><?=$CONFIG['title']; ?></a>
		<a href="<?=$CONFIG['root']; ?>?do=search" ><?= t('Rechercher');?></a>
<?php if(checklogin()): ?>
		<a href="<?=$CONFIG['root']; ?>?do=upload" ><?= t('Téléversement');?></a>
		<a href="<?=$CONFIG['root']; ?>?do=index" ><?= t('Plan du site');?></a>
		<a href="<?=$CONFIG['root']; ?>?do=history" ><?= t('Changements récents');?></a>
		<a href="<?=$CONFIG['root']; ?>?do=config" ><?= t('Configuration');?></a>
		<a href="<?=$CONFIG['root']; ?>?do=login" ><?= t('Déconnexion');?></a>
<?php else: ?>
		<a href="<?=$CONFIG['root']; ?>?do=login&r=<?=$_GET['r'];?>" ><?= t('Connexion');?></a>
<?php endif; ?>
	</nav>
	<section>
	<?php include 'app/inc/app.php'; ?>

	</section>
	<footer>
		<p><?= sprintf(t('Conçu avec %s'), '<a href="https://framagit.org/qwertygc/Wiki" >🦄 Unicorn Wiki</a>');?> </p>
	<footer>
	<script src="app/assets/easymde.js"></script>
</body>
</html>
