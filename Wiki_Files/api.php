<?php

session_start();
include 'app/inc.php';
$config = read_config();

if (isset($_GET['do'])) {
    switch ($_GET['do']) {
        case 'upload':
            ##### UPLOAD ######
            // curl -i -X POST -H "Content-Type: multipart/form-data"  -F "image=@4ba9b82ec3b76fff.png" http://qwerty.legtux.org/varia/wiki/api.php?do=upload
            if (check(@$_SESSION['login']) || check($config['openwrite'])) {
                if (isset($_FILES['image'])) {
                    $uploaddir = 'data/files/';
                    $filename = int_to_alph(time()).'_'.basename($_FILES['image']['name']);
                    @rename($_FILES['image']['name'], 'data/files/'.$filename);
                    //$filename = basename($_FILES['image']['name']);
                    $uploadfile = $uploaddir.$filename;
                    if (in_array($_FILES['image']['type'], $blacklist_mimetype)) {
                        echo json_encode(array('error'=>'typeNotAllowed'));
                    }
                    if (!is_uploaded_file($_FILES['image']['tmp_name'])) {
                        echo json_encode(array('error'=>'importError'));
                        echo "can't upload";
                    }
                    if (move_uploaded_file($_FILES['image']['tmp_name'], $uploadfile)) {
                        echo json_encode(array('data'=> array('filePath'=>'data/files/'.$filename)));
                    }
                }
            } else {
                exit('error');
            }
        break;
        case 'export':
            if (checklogin()) {
                if (isset($_GET['id'])) {
                    $filename = $_GET['id'].'.md';
                    header("Content-disposition: attachment; filename=$filename");
                    header('Content-type:text/plain;charset=utf-8');
                    echo export($_GET['id']);
                } else {
                    $zip = new ZipArchive();
                    $filename = 'backups/export-'.date('Y-m-d', time()).'.zip';
                    $zip->open($filename, ZipArchive::CREATE);
                    foreach (scandir('data') as $content) {
                        if (!in_array($content, $exclude_scan)) {
                            $zip->addFromString($content.'.md', export($content));
                        }
                    }
                    $zip->close();
                    header("Location: ".$filename);
                }
            }
        break;
        case 'backup':
            if (checklogin()) {
                if (!is_dir('backups')) {
                    mkdir('backups');
                }
                $filename = 'backups/backup-'.date('YmdHis', time()).'.zip';
                ZipDir($filename, 'data/');
                mylog('Backup: '.$filename);
                header("Location: ".$filename);
            }
        break;
        case 'feed':
            header('Content-Type: application/atom+xml;charset=utf-8');
            $feed = new Unicorn\Atom();
            $feed->title($config['title']);
            $feed->subtitle('');
            $feed->link($config['root']);

            foreach (scandir('data') as $page) {
                if (!in_array($page, array('.','..', 'config.php', 'files', 'log.log', 'index.html', 'favicon.ico', 'style.css'))) {
                    foreach (scandir('data/'.$page) as $file) {
                        if (!in_array($file, array('.','..'))) {
                            $content = read_article($page.'/'.$file);
                            $recentchange[$content['time']] = array('comment' => $content['comment'], 'id'=>$page, 'version'=>$file, 'time'=>$content['time']);
                        }
                    }
                }
            }
        krsort($recentchange);
        $recentchange = array_slice($recentchange, 0, 10);
        foreach ($recentchange as $data) {
            $data['comment'] = ($data['comment'] == '') ? '-' : $data['comment'];
            $data['title'] = ($data['title'] == '') ? $data['id'] : $data['title'];
            $feed->startentry();
            $feed->title($data['title']);
            //$feed->link($config['root'].'/'.$data['id']);
            $feed->link($config['root'].'/?id='.$data['id'].'&amp;view='.$data['version'].'&amp;do=history');
            $feed->published($data['time']);
            $feed->content($data['comment']);
            $feed->endentry();
        }
        break;
        case 'translate':
            if (isset($_GET['output']) and $_GET['output'] == 'json') {
                header('Content-type:application/json;charset=utf-8');
                ksort($lang[$_GET['lang']]);
                echo json_encode($lang[$_GET['lang']], JSON_PRETTY_PRINT);
            } else {
                if (checklogin()) {
                    if (!isset($_GET['lang'])) {
                        redirect($config['root'].'/api.php?do=translate&lang=fr');
                    }

                    if (!file_exists('app/i18n/'.$_GET['lang'].'.json') and array_key_exists($_GET['lang'], $lang_list)) {
                        copy('app/i18n/fr.json', 'app/i18n/'.$_GET['lang'].'.json');
                        redirect($config['root'].'/api.php?do=translate&lang='.$_GET['lang'].'&src=fr');
                    } else {
                        echo '<!doctype html><html><head><link href="app/assets/style.css" type="text/css" rel="stylesheet" /><link href="data/style.css" type="text/css" rel="stylesheet" /><title>'.t('translation').'</title></head><body><nav></nav><section><a href="'.$config['root'].'/api.php?do=translate&lang='.$_GET['lang'].'&output=json">JSON</a>';
                        $deleted_key = array_diff_key($lang[$_GET['lang']], $lang['fr']);
                        if (!empty($deleted_key)) {
                            echo '<div class="block">'.t('dev.deletedkey');
                            echo '<table><tr><th></th><th>'.t('translation').'</th></tr>';
                            foreach ($deleted_key as $k=>$v) {
                                echo '<tr><td>'.$k.'</td><td>'.$v.'</td></tr>';
                            }
                            echo '</table></div>';
                        }
                        $langfile = json_decode(file_get_contents('app/i18n/'.$_GET['lang'].'.json'), true);
                        $lang[$_GET['lang']] = array_merge($lang[$_GET['lang']], array_diff_key(array_fill_keys(array_keys($lang['fr']), ''), $lang[$_GET['lang']]));
                        $_GET['src'] = isset($_GET['src']) ? $_GET['src'] : 'fr';
                        ksort($lang[$_GET['lang']]);



                        $form = new Unicorn\form(array('method'=>'post', 'action'=>$config['root'].'/api.php?do=translate&lang='.$_GET['lang']));
                        $form->label('lang_name', t('lang.addkey'));
                        $form->input(array('type'=>'text', 'name'=>'newkey', 'value'=>''));
                        $form->input(array('type'=>'hidden', 'name'=>'lang', 'value'=>$_GET['lang']));
                        $form->input(array('type'=>'submit', 'name'=>'submit_newkey', 'value'=>'Submit'));
                        $form->endform();
                        if (isset($_POST['submit_newkey'])) {
                            $addkey = array();
                            $addkey['lang.name'] = $langfile['lang.name'];
                            $addkey['lang.maintainer'] = $langfile['lang.maintainer'];
                            foreach ($langfile['translation'] as $k => $v) {
                                $addkey['translation'][$k] = $v;
                            }
                            $addkey['translation'][$_POST['newkey']] = null;
                            file_put_contents('app/i18n/'.$_GET['lang'].'.json', json_encode($addkey, JSON_PRETTY_PRINT));
                        }




                        $form = new Unicorn\form(array('method'=>'post', 'action'=>$config['root'].'/api.php?do=translate&lang='.$_GET['lang']));
                        $form->startfieldset(t('config'));
                        $form->label('lang_name', t('lang.name'));
                        $form->input(array('type'=>'text', 'name'=>'lang_name', 'value'=>$langfile['lang.name']));
                        $form->label('lang_maintainer', t('lang.maintainer'));
                        $form->input(array('type'=>'text', 'name'=>'lang_maintainer', 'value'=>$langfile['lang.maintainer']));
                        $form->endfieldset();
                        $form->startfieldset(t('translation'));
                        echo '<table><tr><th></th><th></th><th>'.t('translation').'</th></tr>';
                        foreach ($lang[$_GET['lang']] as $key=>$value) {
                            if (!array_key_exists($key, $deleted_key)) {
                                $mark = ($value == '') ? 'style="background-color:yellow;"' : '';
                                echo '<tr '.$mark.'><td>';
                                $form->label($key, $key);
                                echo '</td><td>';
                                $form->textarea(array('disabled'=>'disabled'), htmlspecialchars($lang[$_GET['src']][$key]));
                                echo '</td><td>';
                                $form->textarea(array('name'=>'chain['.$key.']'), htmlspecialchars($value));
                                echo '</td></tr>';
                            }
                        }
                        echo '</table>';
                        $form->input(array('type'=>'hidden', 'name'=>'lang', 'value'=>$_GET['lang']));
                        $form->input(array('type'=>'submit', 'name'=>'submit_translate', 'value'=>'Submit'));
                        $form->endfieldset();
                        $form->endform();
                        if (isset($_POST['submit_translate'])) {
                            $translated = array();
                            $translated['lang.name'] = $_POST['lang_name'];
                            $translated['lang.maintainer'] = $_POST['lang_maintainer'];
                            foreach ($_POST['chain'] as $k=>$v) {
                                $translated['translation'][$k] = htmlspecialchars_decode($v);
                            }
                            ksort($translated);
                            file_put_contents('app/i18n/'.$_GET['lang'].'.json', json_encode($translated, JSON_PRETTY_PRINT));
                            redirect($config['root'].'/api.php?do=translate&lang='.$_GET['lang']);
                        }
                        echo '</section></body></html>';
                    }
                }
            }
        break;
        case 'hour':
        echo '<html><head><meta http-equiv="refresh" content="1"></head><body>'.date('H:i:s', time()).'</body></html>';
        break;
        case 'dev':
        break;
        case 'chocolatine':
            echo '<!doctype html><html><head><link href="app/assets/style.css" type="text/css" rel="stylesheet" /><link href="data/style.css" type="text/css" rel="stylesheet" /><title>Easter egg</title></head><body><section>';
            echo a("javascript:alert('Copain !');", 'Chocolatine', 'class="button"').'          ';
            echo a('https://www.youtube.com/watch?v=dQw4w9WgXcQ', 'Pain au chocolat', 'class="button"').'          ';
            echo a("javascript:alert('Copain !');", 'Couque au chocolat', 'class="button"').'          ';
            echo '</section></body></html>';
        break;
        case 'update':
            if (checklogin()) {
                updater();
            }
        break;
    }
} else {
    redirect($config['root']);
}
